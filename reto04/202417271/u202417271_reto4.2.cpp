#include <iostream>

using namespace std;

int** crearMatriz(int n, int m) {
    int** matriz = new int* [n];

    for (int i = 0; i < n; ++i) {
        matriz[i] = new int[m](); 
    }

    return matriz;
}

void imprimirMatriz(int** matriz, int n, int m) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cout << matriz[i][j] << " ";
        }
        cout << endl;
    }
}

void liberarMatriz(int** matriz, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] matriz[i];
    }

    delete[] matriz;
}

int main() {
    int n, m;
    cout << "Ingrese el n�mero de filas: ";
    cin >> n;
    cout << "Ingrese el n�mero de columnas: ";
    cin >> m;

    int** matriz = crearMatriz(n, m);

    cout << "La matriz creada es:" << endl;
    imprimirMatriz(matriz, n, m);

    liberarMatriz(matriz, n);

    system("pause");

    return 0;
}
