#include <iostream>
#include <random>
#include <cstdlib>
#include <ctime>

using namespace std;




int **matriz(int *n, int *m) {
	int** matriz = new int* [*n];
	for (int i = 0; i < *n; ++i) {
		matriz[i] = new int [*m];
		for (int j = 0; j < *m; ++i) {
			matriz[i][j] = 0;
		}
	}

	return matriz; 
}


void showmatriz(int n, int m, int ** matriz) {

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << matriz[i][j];
			cout << "\n";
		}
	}
}


int main() {

	int** matrix;
	int *n = new int;
	int *m = new int;

	cout << "Ingrese n:";
	cin >>*n; 

	cout << "Ingrese m:";
	cin >> *m;

    matrix = matriz(n, m);
	showmatriz( *n, *m, matrix);


	delete n;
	delete m;


	system("pause");
	return 0;
}