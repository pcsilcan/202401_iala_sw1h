#include "pch.h"
#include <iostream>


using namespace std;
using namespace System;

int** crearMatriz(int filas, int columnas) {
	int** matriz = new int* [filas];

	for (int i = 0; i < filas; i++)
	{
		matriz[i] = new int[columnas];
		for (int j = 0; j < columnas; j++)
		{
			matriz[i][j] = 0; // inicializar en 0
		}
	}
	return matriz;
}
void imprimirmatriz(int** matriz, int filas, int columnas) {
	for (int i = 0; i < filas; i++)
	{
		for (int j = 0; j < columnas; j++)
		{
			cout << matriz[i][j] << " ";
		}
		cout << endl;
	}
}
void liberarMatriz(int** matriz, int filas) {

	for (int i = 0; i < filas; i++)
	{
		delete[] matriz[i];
	}
	delete[] matriz;
}

int main(){
	int filas, columnas;
	cout << "Ingrese el numero de filas: ";
	cin >> filas;
	cout << "Ingrese el numero de columnas: ";
	cin >> columnas;

	int** matriz = crearMatriz(filas, columnas);
	cout << "Matriz inicializada con ceros: " << endl;
	imprimirmatriz(matriz, filas, columnas);
	liberarMatriz(matriz, filas);
    return 0;
}


